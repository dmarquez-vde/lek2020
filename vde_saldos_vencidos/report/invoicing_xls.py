# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: Jesni Banu(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from datetime import date, time, timedelta
import datetime
import string
import ast
from odoo.exceptions import UserError, ValidationError
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx

class InvoicingReportXls(ReportXlsx):

    def get_invoice_lines(self, data):
        
        if data['form'].get('account_client') and not (data['form'].get('date_start') and not data['form'].get('date_end')):
            #print"SI ESTA ENTRANDO CUANDO DETECTA QUE SE INGRESO CLIENTE"
            if data.get('form',False) and data['form'].get('account_client', False):
                invoices = []
                lines = []
                invoice_ids = self.env['account.invoice'].search([
                    ('partner_id','=',data['form'].get('account_client')),
                    ('state','in',['open']),
                    ('type','=','out_invoice')
                    ])
                if len(invoice_ids)==0:
                    raise ValidationError("No se encontraron facturas con el cliente seleccionado")
                for invoice_id in invoice_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    invoices.append(invoice_id.id)

                invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','in',invoices)])
                #print "ESTO EES invoice_ids",invoice_ids
                for line in invoice_line_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    
                    
                    vals = {
                        'invoiceid': line.invoice_id.id,
                        'client': line.invoice_id.partner_id.name,
                        'concept': line.invoice_id.tax_line_ids.name,
                        'coin': line.invoice_id.currency_id.name,
                        'tc': 1/line.invoice_id.tipocambio,
                        'state': line.invoice_id.state,
                        'date_invoice': line.invoice_id.date_invoice,
                        'invoice': line.invoice_id.number,
                        'producto': line.product_id.default_code,
                        'precio': line.price_unit,
                        'cantidad':line.quantity,
                        'unidad': line.uom_id.name,
                        'tax_amount': (line.invoice_id.amount_tax),
                        'total_amount': line.invoice_id.amount_total,
                        'residual' : line.invoice_id.residual,
                        'comercial': line.invoice_id.partner_id.user_id.name,
                        'plazos': line.invoice_id.partner_id.property_payment_term_id.name,
                        'dias': line.invoice_id.partner_id.property_payment_term_id.line_ids.days,
                    }
                    lines.append(vals)

                return lines

        if data['form'].get('date_start') and data['form'].get('date_end') and not data['form'].get('account_client') :
            #print"SI ESTA ENTRANDO CUANDO DETECTA QUE SE INGRESARON FECHAS"
            if data.get('form',False) and data['form'].get('date_start', False) and data['form'].get('date_end', False):
                invoices = []
                lines = []
                invoice_ids = self.env['account.invoice'].search([
                    ('date_invoice','>=',data['form'].get('date_start')),
                    ('date_invoice','<=',data['form'].get('date_end')),
                    ('state','in',['open']),
                    ('type','=','out_invoice')
                    ])
                if len(invoice_ids)==0:
                    raise ValidationError("No se encontraron facturas con el rango de fechas seleccionados")
                #print "ESTO EES invoice_ids",invoice_ids
                for invoice_id in invoice_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    invoices.append(invoice_id.id)

                invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','in',invoices)])
                #print "ESTO EES invoice_ids",invoice_ids
                for line in invoice_line_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    
                    
                    vals = {
                        'invoiceid': line.invoice_id.id,
                        'client': line.invoice_id.partner_id.name,
                        'concept': line.invoice_id.tax_line_ids.name,
                        'coin': line.invoice_id.currency_id.name,
                        'tc': 1/line.invoice_id.tipocambio,
                        'state': line.invoice_id.state,
                        'date_invoice': line.invoice_id.date_invoice,
                        'invoice': line.invoice_id.number,
                        'producto': line.product_id.default_code,
                        'precio': line.price_unit,
                        'cantidad':line.quantity,
                        'unidad': line.uom_id.name,
                        'tax_amount': (line.invoice_id.amount_tax),
                        'total_amount': line.invoice_id.amount_total,
                        'residual' : line.invoice_id.residual,
                        'comercial': line.invoice_id.partner_id.user_id.name,
                        'plazos': line.invoice_id.partner_id.property_payment_term_id.name,
                        'dias': line.invoice_id.partner_id.property_payment_term_id.line_ids.days,
                    }
                    lines.append(vals)

                return lines




        if data['form'].get('date_start') and data['form'].get('date_end') and data['form'].get('account_client'):
            #print"SI ESTA ENTRANDO CUANDO DETECTA QUE SE INGRESARON las 3 cosas"
            if data.get('form',False) and data['form'].get('date_start', False) and data['form'].get('date_end', False) and data['form'].get('account_client', False):
                invoices = []
                lines = []
                invoice_ids = self.env['account.invoice'].search([
                    ('date_invoice','>=',data['form'].get('date_start')),
                    ('date_invoice','<=',data['form'].get('date_end')),
                    ('partner_id','=',data['form'].get('account_client')),
                    ('state','in',['open']),
                    ('type','=','out_invoice')
                    ])
                if len(invoice_ids)==0:
                    raise ValidationError("No se encontraron facturas con el rango de fechas y cliente seleccionados")
                #print "ESTO EES invoice_ids",invoice_ids
                for invoice_id in invoice_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    invoices.append(invoice_id.id)

                invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','in',invoices)])
                #print "ESTO EES invoice_ids",invoice_ids
                for line in invoice_line_ids:
                    #print "esto es INVOICE_ID", invoice_id
                    
                    
                    vals = {
                        'invoiceid': line.invoice_id.id,
                        'client': line.invoice_id.partner_id.name,
                        'concept': line.invoice_id.tax_line_ids.name,
                        'coin': line.invoice_id.currency_id.name,
                        'tc': 1/line.invoice_id.tipocambio,
                        'state': line.invoice_id.state,
                        'date_invoice': line.invoice_id.date_invoice,
                        'invoice': line.invoice_id.number,
                        'producto': line.product_id.default_code,
                        'precio': line.price_unit,
                        'cantidad':line.quantity,
                        'unidad': line.uom_id.name,
                        'tax_amount': (line.invoice_id.amount_tax),
                        'total_amount': line.invoice_id.amount_total,
                        'residual' : line.invoice_id.residual,
                        'comercial': line.invoice_id.partner_id.user_id.name,
                        'plazos': line.invoice_id.partner_id.property_payment_term_id.name,
                        'dias': line.invoice_id.partner_id.property_payment_term_id.line_ids.days,
                    }
                    lines.append(vals)

                return lines

    def get_nc(self,invoice):
        print "nc xxxxxxxxxXXXXXXXXXXXXxxxxxxxxxxxx",invoice
        nc_ = []
        obj = self.env['account.invoice'].search([('id','=',invoice)])
        print "Esto es obj",obj.origin
        algo = self.env['stock.picking'].search([('name','=',obj.origin)])
        print "esto es algo.name",algo.name
        if algo:
            print "nc xxxxxxxxxXXXXXXXXXXXXxxxxxxxxxxxx",algo
            algo1 = self.env['sale.order'].search([('name','=',algo.origin)])
            print "Esto es algo1.name",algo1.name
            for nc in algo1:
                vals = {
                        
                    'client': nc.partner_real_id.name,

                    }
                nc_.append(vals)
        else:
            algo1 = self.env['sale.order'].search([('name','=',obj.origin)])
            if algo1:
                for nc in algo1:
                    vals = {
                            
                        'client': nc.partner_real_id.name,

                        }
                    nc_.append(vals)

        return nc_

    def generate_xlsx_report(self, workbook, data, lines):
        ##print "entra a generate_xls"
        sheet = workbook.add_worksheet('Invoicing Info')
        format10nn = workbook.add_format({'font_size': 15, 'align': 'center', 'bold': True})
        format10nnn = workbook.add_format({'font_size': 12, 'align': 'center', 'bold': True, 'bg_color': 'gray'})
        format1 = workbook.add_format({'font_size': 14, 'bottom': True, 'right': True, 'left': True, 'top': True, 'align': 'vcenter', 'bold': True})
        format11 = workbook.add_format({'font_size': 12, 'align': 'center', 'right': True, 'left': True, 'bottom': True, 'top': True, 'bold': True})
        format10n = workbook.add_format({'bg_color': 'gray', 'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': True})
        format10 = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False})
        format10cur = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False,'num_format':'$#,##0.00'})
        format3 = workbook.add_format({'bottom': True, 'top': True, 'font_size': 12})
        font_size_8 = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8})
        red_mark = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8,
                                        'bg_color': 'red'})
        justify = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 12})
        format3.set_align('center')
        font_size_8.set_align('center')
        justify.set_align('justify')
        format1.set_align('center')
        red_mark.set_align('center')
        fecha_hoy = str(datetime.date.today())



        lines = self.get_invoice_lines(data)
        #print "esto es lines ", lines
        ##print "esto es formato ", format10
        total= 0
        total1 = 0
        total2 = 0 
        total3 = 0
        total4 = 0
        total5 = 0
        ren = 4  
        col = 0
        reg= 0
        number=""
        for line in lines:
            #print"PASANDO AQUI"
            if line['client'] == 'Venta Mostrador':

                sheet.write(ren, col, line['client'], format10)
                print "Esto es invoiceid",line['invoiceid']
                pagos_ = self.get_nc(line['invoiceid'])
                if pagos_:
                    for pagos_l in pagos_:
                        if pagos_l['client'] == False:
                            sheet.write(ren, col + 1, 'No asignado', format10)
                        else:
                            sheet.write(ren, col + 1, pagos_l['client'], format10)
                else:
                    sheet.write(ren, col + 1, 'No relacionado a un pedido', format10)
                if line['comercial'] == False:
                    sheet.write(ren, col + 2, 'No definido', format10)
                else:
                    sheet.write(ren, col + 2, line['comercial'], format10)

                
                sheet.write(ren, col + 3, line['invoice'], format10)
                sheet.write(ren, col + 4, line['date_invoice'], format10)
                sheet.write(ren, col + 5, line['producto'], format10)
                sheet.write(ren, col + 6, line['precio'], format10cur)
                sheet.write(ren, col + 7, line['cantidad'], format10)
                sheet.write(ren, col + 8, line['unidad'], format10)
                sheet.write(ren, col + 9, line['coin'], format10cur)
                if(line['plazos']== False):
                    sheet.write(ren, col + 10, 'No definido', format10)
                else:
                    sheet.write(ren, col + 10, line['plazos'], format10)


                sheet.write(ren, col + 11, line['total_amount'], format10cur)
                total = line['total_amount'] + total
                
                dias_plazo = line['dias']
                dias = timedelta(days=dias_plazo)
                fecha_factura = line['date_invoice']
                a,b,c=fecha_factura.split("-")
                fecha=date(int(a),int(b),int(c))
                fecha_1 = fecha + dias
                print "Esto es fecha",fecha
                print "ESTO ES FECHA_1",fecha_1
                hoy= date.today()
                dias_transcurridos=hoy-fecha_1
                total_factura = line['total_amount'] - 5
                print "ESTO ES DIAS dias_transcurridos",dias_transcurridos

                if dias_transcurridos.days < 1:
                    sheet.write(ren, col + 12, line['residual'], format10cur)
                    total1 = line['residual'] + total1
                    sheet.write(ren, col + 13, '', format10cur)
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days <= 8 and dias_transcurridos.days >= 1:
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, line['residual'], format10cur)
                    total2 = line['residual'] + total2
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days <= 16 and dias_transcurridos.days >= 9 :
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14, line['residual'], format10cur)
                    total3 = line['residual'] + total3
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura :
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days  <= 30 and dias_transcurridos.days >= 17 :
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, line['residual'], format10cur)
                    total4 = line['residual'] + total4
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days >= 31:
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14,'', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, line['residual'], format10cur)
                    total5 = line['residual'] + total5
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)
                ren = ren + 1
                reg = reg + 1
            
            else:    
                sheet.write(ren, col, line['client'], format10)
                sheet.write(ren, col + 1, '', format10)
                if line['comercial'] == False:
                    sheet.write(ren, col + 2, 'No definido', format10)
                else:
                    sheet.write(ren, col + 2, line['comercial'], format10)

                
                sheet.write(ren, col + 3, line['invoice'], format10)
                sheet.write(ren, col + 4, line['date_invoice'], format10)
                sheet.write(ren, col + 5, line['producto'], format10)
                sheet.write(ren, col + 6, line['precio'], format10cur)
                sheet.write(ren, col + 7, line['cantidad'], format10)
                sheet.write(ren, col + 8, line['unidad'], format10)
                sheet.write(ren, col + 9, line['coin'], format10cur)
                if(line['plazos']== False):
                    sheet.write(ren, col + 10, 'No definido', format10)
                else:
                    sheet.write(ren, col + 10, line['plazos'], format10)


                sheet.write(ren, col + 11, line['total_amount'], format10cur)
                total = line['total_amount'] + total
                
                dias_plazo = line['dias']
                dias = timedelta(days=dias_plazo)
                fecha_factura = line['date_invoice']
                a,b,c=fecha_factura.split("-")
                fecha=date(int(a),int(b),int(c))
                fecha_1 = fecha + dias
                print "Esto es fecha",fecha
                print "ESTO ES FECHA_1",fecha_1
                hoy= date.today()
                dias_transcurridos=hoy-fecha_1
                total_factura = line['total_amount'] - 5
                print "ESTO ES DIAS dias_transcurridos",dias_transcurridos

                if dias_transcurridos.days < 1:
                    sheet.write(ren, col + 12, line['residual'], format10cur)
                    total1 = line['residual'] + total1
                    sheet.write(ren, col + 13, '', format10cur)
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days <= 8 and dias_transcurridos.days >= 1:
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, line['residual'], format10cur)
                    total2 = line['residual'] + total2
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days <= 16 and dias_transcurridos.days >= 9 :
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14, line['residual'], format10cur)
                    total3 = line['residual'] + total3
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura :
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days  <= 30 and dias_transcurridos.days >= 17 :
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14, '', format10)
                    sheet.write(ren, col + 15, line['residual'], format10cur)
                    total4 = line['residual'] + total4
                    sheet.write(ren, col + 16, '', format10)
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)

                if dias_transcurridos.days >= 31:
                    sheet.write(ren, col + 12, '', format10cur)
                    sheet.write(ren, col + 13, '', format10)
                    sheet.write(ren, col + 14,'', format10)
                    sheet.write(ren, col + 15, '', format10)
                    sheet.write(ren, col + 16, line['residual'], format10cur)
                    total5 = line['residual'] + total5
                    if line['residual'] < total_factura:
                        sheet.write(ren, col + 17, 'TIENE UN ABONO', format10)
                    else:
                        sheet.write(ren, col + 17, 'SIN ABONO', format10)
                ren = ren + 1
                reg = reg + 1
        
        if line['client'] == 'Venta Mostrador':
            ren = ren + 1
            reg = reg + 1
            sheet.write(ren, col + 10, 'Totales:', format10nnn)
            sheet.write(ren, col + 11, total, format10cur)
            sheet.write(ren, col + 12,total1, format10cur)
            sheet.write(ren, col + 13, total2, format10cur)
            sheet.write(ren, col + 14, total3, format10cur)
            sheet.write(ren, col + 15, total4, format10cur)
            sheet.write(ren, col + 16, total5, format10cur)
            sheet.merge_range('B1:P1', 'REPORTE DE SALDOS VENCIDOS AL DIA' + ' '  + fecha_hoy, format10nn)
            sheet.merge_range('A3:A4', 'CLIENTE', format10nnn)
            sheet.merge_range('B3:B4', 'CLIENTE REAL', format10nnn)
            sheet.merge_range('C3:C4', 'VENDEDOR', format10nnn)
            sheet.merge_range('D3:D4', 'FOLIO FACTURA', format10nnn)
            sheet.merge_range('E3:E4', 'FECHA FACTURA', format10nnn)
            sheet.merge_range('F3:F4', 'PRODUCTO', format10nnn)
            sheet.merge_range('G3:G4', 'PRECIO', format10nnn)
            sheet.merge_range('H3:H4', 'CANTIDAD', format10nnn)
            sheet.merge_range('I3:I4', 'UNIDAD', format10nnn)
            sheet.merge_range('J3:J4', 'MONEDA', format10nnn)
            sheet.merge_range('K3:K4', 'PLAZO DE CREDITO', format10nnn)
            sheet.merge_range('L3:L4', 'IMPORTE FACTURA', format10nnn)
            sheet.merge_range('M3:M4', 'NO VENCIDAS', format10nnn)
            sheet.merge_range('N3:R3', 'SALDO VENCIDO / DIAS', format10nnn)
            sheet.write(3,13,'1-8 DIAS',format10nnn)
            sheet.write(3,14,'9-16 DIAS',format10nnn)
            sheet.write(3,15,'17-30 DIAS',format10nnn)
            sheet.write(3,16,'> 30 DIAS',format10nnn)
            sheet.write(3,17,'ESTATUS',format10nnn)
            
        else:
            ren = ren + 1
            reg = reg + 1
            sheet.write(ren, col + 9, 'Totales:', format10nnn)
            sheet.write(ren, col + 10, total, format10cur)
            sheet.write(ren, col + 11,total1, format10cur)
            sheet.write(ren, col + 12, total2, format10cur)
            sheet.write(ren, col + 13, total3, format10cur)
            sheet.write(ren, col + 14, total4, format10cur)
            sheet.write(ren, col + 15, total5, format10cur)
            sheet.merge_range('B1:P1', 'REPORTE DE SALDOS VENCIDOS AL DIA' + ' '  + fecha_hoy, format10nn)
            sheet.merge_range('A3:A4', 'CLIENTE', format10nnn)
            sheet.merge_range('B3:B4', 'CLIENTE REAL', format10nnn)
            sheet.merge_range('C3:C4', 'VENDEDOR', format10nnn)
            sheet.merge_range('D3:D4', 'FOLIO FACTURA', format10nnn)
            sheet.merge_range('E3:E4', 'FECHA FACTURA', format10nnn)
            sheet.merge_range('F3:F4', 'PRODUCTO', format10nnn)
            sheet.merge_range('G3:G4', 'PRECIO', format10nnn)
            sheet.merge_range('H3:H4', 'CANTIDAD', format10nnn)
            sheet.merge_range('I3:I4', 'UNIDAD', format10nnn)
            sheet.merge_range('J3:J4', 'MONEDA', format10nnn)
            sheet.merge_range('K3:K4', 'PLAZO DE CREDITO', format10nnn)
            sheet.merge_range('L3:L4', 'IMPORTE FACTURA', format10nnn)
            sheet.merge_range('M3:M4', 'NO VENCIDAS', format10nnn)
            sheet.merge_range('N3:R3', 'SALDO VENCIDO / DIAS', format10nnn)
            sheet.write(3,13,'1-8 DIAS',format10nnn)
            sheet.write(3,14,'9-16 DIAS',format10nnn)
            sheet.write(3,15,'17-30 DIAS',format10nnn)
            sheet.write(3,16,'> 30 DIAS',format10nnn)
            sheet.write(3,17,'ESTATUS',format10nnn)
            


InvoicingReportXls('report.export_lek_invoicinginfo_xls.invoicing_history_report_xls_saldo.xlsx', 'account.invoice')
