# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: Jesni Banu(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api


class InvoicingReport(models.TransientModel):
    _name = "wizard.invoicing.history.saldo"
    _description = "invoicing History of saldo"

    #warehouse = fields.Many2many('stock.warehouse', 'wh_wiz_rel', 'wh', 'wiz', string='Warehouse', required=True)
    #category = fields.Many2many('product.category', 'categ_wiz_rel', 'categ', 'wiz', string='Warehouse')
    date_start = fields.Date(string="Fecha Inicio")
    date_end = fields.Date(string="Fecha Fin")
    account_client = fields.Many2one('res.partner', string="Nombre del cliente")

    @api.onchange('currency_rate')
    def onchange_currency_rate_id(self, currency_rate):
        #print "esto es currency_rate ", currency_rate
        if currency_rate:
            rate=self.env['res.currency.rate'].browse(currency_rate).rate
            #print "esto es rate ", rate
            #self.tc = 111
            return {'value':{'tc':1/rate}}

    @api.multi
    def export_xls_saldo(self):
        #print "entra a export_xls"
        context = self._context
        datas = {'ids': context.get('active_ids', [])}
        datas['model'] = 'hr.attendance'
        datas['form'] = self.read()[0]
        #print "esto es datas ", datas
        for field in datas['form'].keys():
            #print "esto es field ", field
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]
        if context.get('xls_export'):
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'export_lek_invoicinginfo_xls.invoicing_history_report_xls_saldo.xlsx',
                    'datas': datas,
                    'name': 'Saldos Vencidos'
                    }
