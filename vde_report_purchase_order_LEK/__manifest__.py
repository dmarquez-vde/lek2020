# -*- encoding: utf-8 -*-
###########################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
#
#     @Author: David Márquez/VDE dmarquez@vde-suite.com www.vde-suite.com
#
##############################################################################

{
    "name" : "VDE REPORT PURCHASE LEK",
    "version" : "10.0",
    "author" : "VDE-Suite",
    "category" : "",
    "description" : """Custom purchase report for LEK
    """,
    "website" : "http://www.vde-suite.com/",
    "license" : "AGPL-3",
    "depends" : ['base', 'purchase','professional_templates'],
    "init_xml" : [],
    "demo_xml" : [],
    "data" : [

        'reports/purchase_reports.xml',

        'purchase_order/switch_templates.xml',
        'purchase_order/compras_lek.xml',

        #'views/report_style_views.xml',
        #'views/payment.xml',
        ],
    "installable" : True,
    "active" : False,
    "price": 1000,
    "currency": "USD"
}
