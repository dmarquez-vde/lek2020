# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
from odoo.tools.float_utils import float_compare

class AccountInvoice (models.Model):

    _inherit = 'account.invoice'

    crea_picking = fields.Boolean(string="Crea picking desde factura?", default=True)
    picking_id = fields.Many2one('stock.picking', string="Movimiento de almacen")
    picking_status = fields.Selection([
        ('draft', 'Draft'), ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Operation'),
        ('confirmed', 'Waiting Availability'),
        ('partially_available', 'Partially Available'),
        ('assigned', 'Available'), ('done', 'Done')], string="Estado Mov.", related="picking_id.state")

    @api.multi
    def open_picking(self):
        self.ensure_one()
        #domain = [('type_project_id','=',self.id)]
        form_view = self.env.ref('stock.view_picking_form').id
        #kanban_view = self.env.ref('vde_cya.vde_cya_project_view_kanban').id
        if not self.picking_id:
            raise UserError(_('Esta factura no tiene movimiento de almacen.'))
        else:
            return {
                'name': _('%s' % (self.picking_id.name)),
                #'domain': domain,
                'res_model': 'stock.picking',
                'type': 'ir.actions.act_window',
                #'view_id': self.env.ref('vde_cya.vde_cya_project_view_kanban').id,
                'views': [(form_view, 'form')],
                'view_mode': 'form',
                'res_id': self.picking_id.id,
                #'view_type': 'kanban,tree,form',
                #'help': _('''<p class="oe_view_nocontent_create">
                        #Create a new %s.</p><p>
                    #</p>''' % (self.label_tasks)),
                'limit': 80,
                'context': "{'default_res_model': '%s','default_res_id': %d}" % (self._name, self.picking_id.id)
                }

    @api.multi
    def make_picking(self):
        sigue = False
        print "Entra a crea_picking ", self.picking_id
        if self.origin == False:
            raise UserError(_('Solo se puede crear movimento de almacen en facturas relacionadas a PO.'))
        if self.state not in ['open','paid']:
            raise UserError(_('Solo se puede crear movimento de almacen en facturas en estado ABIERTO o PAGADO.'))
        if self.crea_picking == False:
            raise UserError(_('Esta factura no puede crear movimiento de almacen.'))
	if self.picking_id:
            raise UserError(_('Esta factura ya tiene un movimiento de almacen asociado.'))
        else:
            po_id = self.env['purchase.order'].search([('name', '=', self.origin)])
            print "esto es po_id ", po_id
            #print "esto es po_id.picking_ids ", po_id.picking_ids
            #if len(po_id.picking_ids) >= 1:
            picking_ids = self.env['stock.picking'].search([('origin', '=', self.origin)])
            for picking_id in picking_ids:
                print "esto es picking_id ", picking_id
                if picking_id.origin == self.origin:
                    pick_id = picking_id.copy()
                    print "esto es pick_id ", pick_id
                    if pick_id:
                        if picking_id.state == 'assigned':
                            print "estado = assigned"
                            picking_id.action_cancel()
                            print "va a cancelar movimiento"
                        print "estado != assigned"
                        pick_id.write({'origin':self.number})
                        self.picking_id = pick_id
                            #for line in pick_id.move_lines:
                                #if line.unlink():
                                    #sigue=True
                            #if sigue==True:
                        for line in self.invoice_line_ids:
                                #move_id = self.env['stock.move'].create({'picking_id':pick_id.id,'product_id':line.product_id.id, 'name':line.name, 'product_uom_qty':line.quantity, 'product_uom':line.uom_id.id, 'location_id':pick_id.location_id.id, 'location_dest_id':pick_id.location_dest_id.id})
                            move_id0 = self.env['stock.move'].search([('product_id','=', line.product_id.id), ('picking_id','=', picking_id.id),('facturado','=',False)], limit=1)
                            move_id = self.env['stock.move'].search([('product_id','=', line.product_id.id), ('picking_id','=', pick_id.id),('facturado','=',False)], limit=1)
                            print "esto es move_id ", move_id
                            #if move_id0.product_uom_qty < line.quantity:
                                #raise UserError(_('La cantidad facturada es mayor a la cantidad restante por recibir.'))
                            #else:
                            move_id.write({'product_uom_qty':line.quantity, 'facturado':True})
                            move_id0.write({'product_uom_qty':move_id0.product_uom_qty-line.quantity})




    @api.onchange('currency_id', 'date_invoice')
    def _onchange_currency_id(self):
        print "entra a _onchange_currency_id"
        ctx = self.env.context
        print "esto es type ", ctx.get('type')
        if ctx.get('type') == 'out_invoice':
            if self.currency_id:
                for line in self.invoice_line_ids.filtered(lambda r: r.purchase_line_id):
                    line.price_unit = line.purchase_id.currency_id.with_context(date=self.date_invoice).compute(line.purchase_line_id.price_unit, self.currency_id, round=False)

    def _prepare_invoice_line_from_po_line(self, line):
        print "entra a prepare_invoice ", line
        print "esto es context ", self.env.context
        if line.product_id.purchase_method == 'purchase':
            qty = line.product_qty - line.qty_invoiced
        else:
            qty = line.qty_received - line.qty_invoiced
        if float_compare(qty, 0.0, precision_rounding=line.product_uom.rounding) <= 0:
            qty = 0.0
        taxes = line.taxes_id
        invoice_line_tax_ids = line.order_id.fiscal_position_id.map_tax(taxes)
        invoice_line = self.env['account.invoice.line']
        data = {
            'purchase_line_id': line.id,
            'name': line.order_id.name+': '+line.name,
            'origin': line.order_id.origin,
            'uom_id': line.product_uom.id,
            'product_id': line.product_id.id,
            'account_id': invoice_line.with_context({'journal_id': self.journal_id.id, 'type': 'in_invoice'})._default_account(),
            #'price_unit': line.order_id.currency_id.with_context(date=self.date_invoice).compute(line.price_unit, self.currency_id, round=False),
            'price_unit': line.price_unit,
            'quantity': qty,
            'discount': 0.0,
            'account_analytic_id': line.account_analytic_id.id,
            'analytic_tag_ids': line.analytic_tag_ids.ids,
            'invoice_line_tax_ids': invoice_line_tax_ids.ids
        }
        account = invoice_line.get_invoice_line_account('in_invoice', line.product_id, line.order_id.fiscal_position_id, self.env.user.company_id)
        if account:
            data['account_id'] = account.id
        print "esto es data ", data
        return data

    # Load all unsold PO lines
    @api.onchange('purchase_id')
    def purchase_order_change(self):
        if not self.purchase_id:
            return {}
        if not self.partner_id:
            self.partner_id = self.purchase_id.partner_id.id

        new_lines = self.env['account.invoice.line']
        for line in self.purchase_id.order_line - self.invoice_line_ids.mapped('purchase_line_id'):
            data = self._prepare_invoice_line_from_po_line(line)
            new_line = new_lines.new(data)
            print "esto es new_line ", new_line.price_unit
            new_line._set_additional_fields(self)
            new_lines += new_line
        print "esto es new_lines ", new_lines
        self.invoice_line_ids += new_lines
        self.purchase_id = False
        return {}


    @api.onchange('journal_id')
    def _onchange_journal_id(self):
        ctx = {}
        print "esto es self.env ", self.env.context
        ctx = self.env.context
        po_id = ctx.get('default_purchase_id')
        po = self.env['purchase.order'].browse(po_id)
        if po.currency_id:
            self.currency_id = po.currency_id.id or self.journal_id.company_id.currency_id.id
        else:
            if self.journal_id:
                self.currency_id = self.journal_id.currency_id.id or self.journal_id.company_id.currency_id.id


class StockMove (models.Model):

    _inherit = 'stock.move'

    facturado = fields.Boolean(string="Facturado?", default=False)

