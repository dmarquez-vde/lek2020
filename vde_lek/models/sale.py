# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
from odoo.tools.float_utils import float_compare

class SaleOrder (models.Model):

    _inherit = 'sale.order'
    
    invoice_ids = fields.Many2many("account.invoice", string='Invoices', schange_default=True, compute="_get_invoiced", index=True, store=True)
    partner_real_id = fields.Many2one('res.partner', string='Cliente real')
    contact_id = fields.Many2one('res.partner', string="Atencion a")
    tcosto = fields.Char(string="TC/Costo", store=True)
    date_delivery = fields.Date('Fecha de Entrega')
    proveedor = fields.Many2one('res.partner', string='Proveedor', schange_default=True, index=True, track_visibility='always')
    country_id = fields.Many2one('res.country', string="Destino (Pais)", default=157)
    state_id = fields.Many2one('res.country.state', string="Destino (Estado)")
    city_id = fields.Many2one('res.country.state.city', string="Destino (Municipio)")

