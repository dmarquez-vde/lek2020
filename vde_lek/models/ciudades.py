import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp


class ResCountryStateCity(models.Model):

    _name = 'res.country.state.city'
    code = fields.Char(string="Codigo Municipio")
    name = fields.Char(string="Nombre del Municipio")
    state_id = fields.Many2one('res.country.state', string="Provincia")
