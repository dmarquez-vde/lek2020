import json
from lxml import etree
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.tools import float_is_zero
from odoo.tools.misc import formatLang
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import odoo.addons.decimal_precision as dp


class VdeCfdiDestination(models.Model):

    _name = 'vde_cfdi_33.destination'
    name = fields.Char(string="Destino")
