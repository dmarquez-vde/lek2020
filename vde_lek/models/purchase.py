# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
from odoo.tools.float_utils import float_compare

class PurchaseOrderLine(models.Model):

    _inherit = 'purchase.order.line'

    contract_id = fields.Many2one('purchase.contract', string="Contrato de compra")
    property_line_id = fields.Many2one('purchase.contract.line.property', string="Propiedades")
    property_lek_cost = fields.Float(string="Costo Caracteristica")

    @api.multi
    def open_order(self):
        self.ensure_one()
        #domain = [('type_project_id','=',self.id)]
        form_view = self.env.ref('purchase.purchase_order_form').id
        #kanban_view = self.env.ref('vde_cya.vde_cya_project_view_kanban').id
        return {
            'name': _('%s' % (self.order_id.name)),
            #'domain': domain,
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            #'view_id': self.env.ref('vde_cya.vde_cya_project_view_kanban').id,
            'views': [(form_view, 'form')],
            'view_mode': 'form',
            'res_id': self.order_id.id,
            #'view_type': 'kanban,tree,form',
            #'help': _('''<p class="oe_view_nocontent_create">
                        #Create a new %s.</p><p>
                    #</p>''' % (self.label_tasks)),
            'limit': 80,
            'context': "{'default_res_model': '%s','default_res_id': %d}" % (self._name, self.order_id.id)
            }


    @api.depends('product_qty', 'price_unit', 'taxes_id', 'property_lek_cost')
    def _compute_amount(self):
        for line in self:
            taxes = line.taxes_id.compute_all(line.price_unit, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id, price_lek=line.property_lek_cost)
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    @api.onchange('property_line_id')
    def onchange_property_lek(self, propiedad):
        return {'value':{'property_lek_cost':self.env['purchase.contract.line.property'].browse(propiedad).price or 0}}

    @api.onchange('product_qty')
    def onchange_product_qty(self):
        result = {}
        available_qty = 0
        if self.contract_id:
            for line in self.contract_id.contract_line:
                if line.product_id == self.product_id:
                    available_qty = available_qty + line.remaning_qty
            if self.product_qty > available_qty:
                text = "Solo hay "+str(available_qty)+" "+str(self.product_uom.name)+" de "+str(self.product_id.name)+" disponible en el contrato: "+str(self.contract_id.name)
                raise UserError(_(text) )
            else:
                return result
        else:
            return result


    @api.onchange('contract_id')
    def onchange_contract_id(self):
        result = {}
        products = []
        if not self.contract_id:
            return result
        for lines in self.contract_id.contract_line:
            products.append(lines.product_id.id)
        domain = {'product_id': [('id', 'in', products)]}
        result = {'domain': domain}
        return result

    #@api.model
    #def create(self, vals):
        #record = super(PurchaseOrderLine, self).create(vals)
        #print "esto es record", record
        #if record:
            #contract_line=self.env['purchase.contract.line'].search([('contract_id','=',record.contract_id.id), ('product_id','=',record.product_id.id)])
            #print "Esto es contract_line ", contract_line
            #if contract_line:
                #contract_line.write({'item_ids':[(4,record.id)]})
        #return record

class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'

    @api.multi
    def write(self, vals):
        print "vals ", vals
        if vals.get('order_line'):
            available_qty = 0
            for line in vals.get('order_line'):
                if line[2]:
                    if line[2].get('product_qty'):
                        print "line ", line
                        if line[1]:
                            linea=self.env['purchase.order.line'].browse(line[1])
                            if line[2].get('product_id'):
                                if linea.product_id == line[2].get('product_id'):
                                    for c_line in linea.contract_id.contract_line:
                                        if c_line.product_id==line[2].get('product_id'):
                                            available_qty = available_qty + c_line.remaning_qty
                            else:
                                for c_line in linea.contract_id.contract_line:
                                    if c_line.product_id==linea.product_id:
                                        available_qty = available_qty + c_line.remaning_qty
                            c_line_ids=self.env['purchase.contract.line'].search([('contract_id','=',line[2].get('contract_id')),('product_id','=',line[2].get('product_id'))])
                            for c_line in c_line_ids:
                                #c_line = self.env['purchase.contract.line'].browse(c_line_id)
                                #print "esto es c_line ", c_line
                                available_qty=available_qty + c_line.remaning_qty
        result = super(PurchaseOrder, self).write(vals)
        return result


    @api.multi
    def button_confirm(self):
        for order in self:
            for record in order.order_line:
                contract_line=self.env['purchase.contract.line'].search([('contract_id','=',record.contract_id.id), ('product_id','=',record.product_id.id)])
                print "Esto es contract_line ", contract_line
                if contract_line:
                    contract_line.write({'item_ids':[(4,record.id)]})
        result = super(PurchaseOrder, self).button_confirm()
        return result


    @api.multi
    def button_cancel(self):
        for order in self:
            for record in order.order_line:
                contract_line=self.env['purchase.contract.line'].search([('contract_id','=',record.contract_id.id), ('product_id','=',record.product_id.id)])
                print "Esto es contract_line ", contract_line
                if contract_line:
                    contract_line.write({'item_ids':[(3,record.id, None)]})
        result = super(PurchaseOrder, self).button_cancel()
        return result
