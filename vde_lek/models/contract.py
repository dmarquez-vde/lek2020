# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
from odoo.tools.float_utils import float_compare

class PurchaseContractLineProperty(models.Model):

    _name = 'purchase.contract.line.property'

    line_id = fields.Many2one('purchase.contract.line')
    product_id = fields.Many2one('product.product', string="Producto", related='line_id.product_id', store=True)
    name = fields.Many2one('lek.property', string="Caracteristica")
    price = fields.Float('Precio')

class PurchaseContractLine(models.Model):

    _name = 'purchase.contract.line'

    @api.one
    @api.depends('item_ids.product_qty', 'product_qty')
    def _get_total_qty(self):
        self.total_qty = sum(line.product_qty for line in self.item_ids)
        self.remaning_qty=self.product_qty-self.total_qty

    contract_id = fields.Many2one('purchase.contract', string="Lineas de contrato")
    product_id = fields.Many2one('product.product', string="Producto")
    product_qty = fields.Float(string="Cantidad")
    product_uom = fields.Many2one('product.uom', string="Unidad de Medida")
    item_ids = fields.Many2many('purchase.order.line', 'contract_purchase_rel', 'contract_id', 'line_id', 'Items')
    total_qty = fields.Float(string="Total Cantidad Solicitada", compute=_get_total_qty, store=True)
    remaning_qty = fields.Float(string="Cantidad remanente", compute=_get_total_qty, store=True)
    #property_ids = fields.Many2many('lek.property', 'contract_property_rel', 'contract_id', 'property_id', 'Caracteristicas')
    property_line = fields.One2many('purchase.contract.line.property','line_id', 'Propiedades')

class PurchaseContract(models.Model):

    _name = 'purchase.contract'
    _inherit = ['mail.thread']

    user_id = fields.Many2one('res.users',string='User')
    name = fields.Char(string="Folio")
    partner_id = fields.Many2one('res.partner', string="Proveedor", domain="[('supplier','=',True)]")
    date = fields.Date(string="Fecha")
    comment = fields.Text(string="Comentarios")
    state = fields.Selection([('draft','En borrador'),('open','En proceso'),
                                       ('cancelled', 'Cancelado'),
                                       ('pending','Pendiente'),
                                       ('done','Cerrado')], default='draft')
    contract_line = fields.One2many('purchase.contract.line','contract_id', 'Items')


    @api.model
    def create(self,vals):
        vals.update({'user_id':self._uid})
        #vals.update({'remaning_qty':vals.get('qty')})
        sequence = self.env['ir.sequence'].get('pocontract')
        vals['name']=sequence
        order =  super(models.Model, self).create(vals)
        return order

    def set_draft(self):
        return self.write({'state': 'draft'})

    def set_done(self):
        return self.write({'state': 'done'})

    def set_cancel(self):
        for line in self.contract_line:
            if len(line.item_ids)>0:
                raise UserError(_('No puede cancelar un contrato que tiene partidas relacionadas') )
        return self.write({'state': 'cancelled'})

    def set_pending(self):
        return self.write({'state': 'pending'})

    def set_open(self):
        return self.write({'state': 'open'})
