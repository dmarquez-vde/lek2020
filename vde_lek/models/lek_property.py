# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import math

class lek_property(models.Model):
    _name = 'lek.property'
    _inherit = ['mail.thread']

    name = fields.Char(string="Nombre")
    product_ids = fields.Many2many('product.product', 'propery_product_rel', 'property_id', 'product_id', string="Productos")
    #cost = fields.Float(string="Costo")