# -*- coding: utf-8 -*-
# ##############################################################################
#

#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-Today Acespritech Solutions Pvt Ltd
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import fields,models,api,_
import datetime
from openerp.exceptions import except_orm, Warning, RedirectWarning
from odoo.exceptions import UserError, AccessError
from odoo import _
from odoo.tools.float_utils import float_compare

class ResPartner (models.Model):

    _inherit = 'res.partner'

    partner_cuentas = fields.One2many('res.partner.cuentas','partner_id', string="Cuentas del cliente")

    partner_real_id = fields.Many2one('res.partner', string='Cliente real')

class ResPartnerCuentas (models.Model):

    _name = 'res.partner.cuentas'

    name = fields.Char(string="Cuenta bancaria", compute='_get_cuenta_name')
    partner_id = fields.Many2one('res.partner', string="Cliente")
    banco_emisor = fields.Selection(selection=[('BBA940707IE1', 'BANCO DEL BAJIO'),
     ('BII931004P61', 'BANCO INBURSA'),
     ('BIN931011519', 'BANCO INTERACCIONES'),
     ('BMN930209927', 'BANCO MERCANTIL DEL NORTE'),
     ('BMI9704113PA', 'BANCO MONEX'),
     ('BMI061005NY5', 'BANCO MULTIVA'),
     ('BAF950102JP5', 'BANCA AFIRME'),
     ('BBA830831LJ2', 'BBVA BANCOMER'),
     ('HMI950125KG8', 'HSBC'),
     ('IBA950503GTA', 'IXE BANCO'),
     ('SIN9412025I4', 'SCOTIABANK INVERLAT'),
     ('BSM970519DU8', 'BANCO SANTANDER'),
     ('BNM840515VB1', 'BANCO NACIONAL DE MEXICO'),
     ('BNE820901682', 'BANCO NACIONAL DE EJERCITO FUERZA AEREA Y ARMADA'),
     ('BRM940216EQ6', 'BANCO REGIONAL DE MONTERREY'),
     ('BBA130722BR7', 'BANCO BANCREA'),
     ('BAI0205236Y8', 'BANCO AZTECA')], string=_('Banco emisor'))
    cuenta_emisor = fields.Char('Cuenta del emisor')
    rfc_banco_emisor = fields.Char(_('RFC banco emisor'), compute='_compute_rfc_banco_emisor')
    currency_id = fields.Many2one('res.currency', string="Moneda")

    @api.one
    @api.depends('banco_emisor')
    def _compute_rfc_banco_emisor(self):
        if self.banco_emisor:
            self.rfc_banco_emisor = self.banco_emisor

    @api.one
    @api.depends('banco_emisor','cuenta_emisor','currency_id')
    def _get_cuenta_name(self):
        if self.banco_emisor:
            self.name = str(self.banco_emisor) + " - " + str(self.cuenta_emisor) + " - " + str(self.currency_id.name)