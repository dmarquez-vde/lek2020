# -*- coding: utf-8 -*-
# License LGPL-3.0 or later (https://opensource.org/licenses/LGPL-3.0).
#
#This software and associated files (the "Software") may only be used (executed,
#modified, executed after modifications) if you have purchased a valid license
#from the authors, typically via Odoo Apps, or if you have received a written
#agreement from the authors of the Software (see the COPYRIGHT section below).
#
#You may develop Odoo modules that use the Software as a library (typically
#by depending on it, importing it and using its resources), but without copying
#any source code or material from the Software. You may distribute those
#modules under the license of your choice, provided that this license is
#compatible with the terms of the Odoo Proprietary License (For example:
#LGPL, MIT, or proprietary licenses similar to this one).
#
#It is forbidden to publish, distribute, sublicense, or sell copies of the Software
#or modified copies of the Software.
#
#The above copyright notice and this permission notice must be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
#IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
#DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
#ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#DEALINGS IN THE SOFTWARE.
#
#########COPYRIGHT#####
# © 2017 Bernard K Too<bernard.too@optima.co.ke>
import os
from odoo import models, fields, api, _
from odoo.tools.safe_eval import safe_eval
from odoo.exceptions import UserError
rotate = [(str(x), str(x) +"°") for x in range(0,361)]
fontsize = [(x, str(x)) for x in range(1,160)]
opacity = [(str(round(x * 0.01, 2)), str(round(x * 0.01, 2))) for x in range(5, 105, 5)]

class TemplateSettings(models.Model):

    _inherit='report.template.settings'

   
    @api.model
    def _default_so_template(self):
        def_tpl = self.env['ir.ui.view'].search([('key', 'like', 'professional_templates.SO\_%\_document' ), ('type', '=', 'qweb')], 
            order='key asc', limit=1)
        return def_tpl or self.env.ref('sale.report_saleorder_document')

    template_so = fields.Many2one('ir.ui.view', 'Order/Quote Template', default=_default_so_template,
                    domain="[('type', '=', 'qweb'), ('key', 'like', '%.SO\_%\_document' )]", required=False)
