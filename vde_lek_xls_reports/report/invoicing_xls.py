# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: Jesni Banu(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import datetime
import string
import ast
from odoo.exceptions import UserError
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx

class InvoicingReportXls(ReportXlsx):

    def get_invoice_lines(self, data):
        if data.get('form',False) and data['form'].get('date_start', False) and data['form'].get('date_end', False):
            invoices = []
            lines = []
            invoice_ids = self.env['account.invoice'].search([
                ('date_invoice','>=',data['form'].get('date_start')),
                ('date_invoice','<=',data['form'].get('date_end')),
                ('state','!=','cancel'),
                ('type','=','out_invoice')
                ])
            for invoice_id in invoice_ids:
                invoices.append(invoice_id.id)
            #print "esto es invoice_ids ", invoices
            invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','in',invoices)])
            #print "esto es invoice_line_ids ", invoice_line_ids
            for line_id in invoice_line_ids:
                #print "esto es currency ", data['form'].get('currency_id')
                if line_id.currency_id.id==data['form'].get('currency_id'):
                    tc = data['form'].get('tc')
                    #print "esto es tc ", tc
                    vals = {
                        'invoiceid': line_id.invoice_id.id,
                        'client': line_id.invoice_id.partner_id.name,
                        'concept': line_id.invoice_id.tax_line_ids.name,
                        'discount': line_id.discount,
                        'coin': line_id.invoice_id.currency_id.name,
                        'tc': 1/line_id.invoice_id.tipocambio,
                        'state': line_id.invoice_id.state,
                        'date_invoice': line_id.invoice_id.date_invoice,
                        'invoice': line_id.invoice_id.number,
                        'qty': line_id.quantity,
                        'product': line_id.product_id.name,
                        'product_code': line_id.product_id.default_code,
                        'price': (line_id.price_unit),
                        'amount': (line_id.price_unit) * (line_id.quantity),
                        'tax_amount': (line_id.invoice_id.amount_tax),
                        'total_amount':line_id.invoice_id.amount_total,
                        'origin':line_id.invoice_id.origin,
                        }
                else:
                    vals = {
			'invoiceid': line_id.invoice_id.id,
                        'client': line_id.invoice_id.partner_id.name,
                        'concept': line_id.invoice_id.tax_line_ids.name,
                        'discount': line_id.discount,
                        'coin': line_id.invoice_id.currency_id.name,
                        'tc': 1/line_id.invoice_id.tipocambio,
                        'state': line_id.invoice_id.state,
                        'date_invoice': line_id.invoice_id.date_invoice,
                        'invoice': line_id.invoice_id.number,
                        'qty': line_id.quantity,
                        'product': line_id.product_id.name,
                        'product_code': line_id.product_id.default_code,
                        'price': (line_id.price_unit),
                        'amount': (line_id.price_unit) * (line_id.quantity),
                        'tax_amount': (line_id.invoice_id.amount_tax),
                        'total_amount': line_id.invoice_id.amount_total,
                        'origin':line_id.invoice_id.origin,
                }
                lines.append(vals)

            return lines
    def get_pagos(self,invoice):
	pagos_ = []
	id_pago=''
	obj = self.env['account.invoice.line.invoice'].search([('invoice_id', '=', invoice)])
	for pago in obj:
	    #print pago
	    invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','=',pago.nc_id.id)])
	    #print "esto es invoice_line_ids ", invoice_line_ids
            for line_id in invoice_line_ids:
		if(line_id.invoice_id.state=='open'):
			id_pago = line_id.invoice_id.number
			#id_pago = string.replace(id_pago,'account.payment(','')
	   		#id_pago = string.replace(id_pago,',)','')
			vals = {
				'idpago':id_pago,
				'comentarios':line_id.invoice_id.comment,
				'describe':line_id.product_id.name,
                        	'discount': line_id.discount,
                        	'amount': line_id.invoice_id.amount_tax,
				'price': (line_id.price_unit),
                        	'tax_amount': (line_id.price_unit) * (line_id.quantity),
				}
	    		pagos_.append(vals)
	return pagos_


    def get_warehouse(self, data):
        if data.get('form', False) and data['form'].get('warehouse', False):
            l1 = []
            l2 = []
            obj = self.env['stock.warehouse'].search([('id', 'in', data['form']['warehouse'])])
            for j in obj:
                l1.append(j.name)
                l2.append(j.id)
        return l1, l2

    def get_category(self, data):
        if data.get('form', False) and data['form'].get('category', False):
            l2 = []
            obj = self.env['product.category'].search([('id', 'in', data['form']['category'])])
            for j in obj:
                l2.append(j.id)
            return l2
        return ''

    def get_lines(self, data, warehouse):
        lines = []
        categ = self.get_category(data)
        if categ:
            stock_history = self.env['product.product'].search([('categ_id', 'in', categ)])
        else:
            stock_history = self.env['product.product'].search([])
        for obj in stock_history:
            sale_value = 0
            purchase_value = 0
            product = self.env['product.product'].browse(obj.id)
            sale_obj = self.env['sale.order.line'].search([('order_id.state', 'in', ('sale', 'done')),
                                                           ('product_id', '=', product.id),
                                                           ('order_id.warehouse_id', '=', warehouse)])
            for i in sale_obj:
                sale_value = sale_value + i.product_uom_qty
            purchase_obj = self.env['purchase.order.line'].search([('order_id.state', 'in', ('purchase', 'done')),
                                                                   ('product_id', '=', product.id),
                                                                   ('order_id.picking_type_id', '=', warehouse)])
            for i in purchase_obj:
                purchase_value = purchase_value + i.product_qty
            available_qty = product.with_context({'warehouse': warehouse}).virtual_available + \
                            product.with_context({'warehouse': warehouse}).outgoing_qty - \
                            product.with_context({'warehouse': warehouse}).incoming_qty
            value = available_qty * product.standard_price
            vals = {
                'sku': product.default_code,
                'name': product.name,
                'category': product.categ_id.name,
                'cost_price': product.standard_price,
                'available': available_qty,
                'virtual': product.with_context({'warehouse': warehouse}).virtual_available,
                'incoming': product.with_context({'warehouse': warehouse}).incoming_qty,
                'outgoing': product.with_context({'warehouse': warehouse}).outgoing_qty,
                'net_on_hand': product.with_context({'warehouse': warehouse}).qty_available,
                'total_value': value,
                'sale_value': sale_value,
                'purchase_value': purchase_value,
            }
            lines.append(vals)
        return lines

    def generate_xlsx_report(self, workbook, data, lines):
        #print "entra a generate_xls"
        sheet = workbook.add_worksheet('Invoicing Info')
        format1 = workbook.add_format({'font_size': 14, 'bottom': True, 'right': True, 'left': True, 'top': True, 'align': 'vcenter', 'bold': True})
        format11 = workbook.add_format({'font_size': 12, 'align': 'center', 'right': True, 'left': True, 'bottom': True, 'top': True, 'bold': True})
        format10n = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': True})
        format10 = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False})
        format10cur = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False,'num_format':'$#,##0.00'})
        format3 = workbook.add_format({'bottom': True, 'top': True, 'font_size': 12})
        font_size_8 = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8})
        red_mark = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8,
                                        'bg_color': 'red'})
        justify = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 12})
        format3.set_align('center')
        font_size_8.set_align('center')
        justify.set_align('justify')
        format1.set_align('center')
        red_mark.set_align('center')
        sheet.write(0,0,'Fecha Factura',format10n)
        sheet.write(0,1,'Folio Factura',format10n)
        sheet.write(0,2,'Pedido',format10n)
        sheet.write(0,3,'Agente Comercial',format10n)
        sheet.write(0,4,'Concepto',format10n)
        sheet.write(0,5,'Cliente',format10n)
        sheet.write(0,6,'Moneda',format10n)
        sheet.write(0,7,'T.C.',format10n)
        sheet.write(0,8,'Estado',format10n)
        sheet.write(0,9,'Codigo Producto',format10n)
        sheet.write(0,10,'Nombre (Producto)',format10n)
        sheet.write(0,11,'Cantidad (toneladas)',format10n)
        sheet.write(0,12,'Precio',format10n)
        sheet.write(0,13,'Neto',format10n)
        sheet.write(0,14,'Descuento(%)',format10n)
        sheet.write(0,15,'Impuesto',format10n)
        sheet.write(0,16,'Total',format10n)
        sheet.write(0,17,'Factura relacionada',format10n)
        sheet.write(0,18,'Descripcion',format10n)
        sheet.write(0,19,'Precio unitario',format10n)
        sheet.write(0,20,'Descuento(%)',format10n)
        sheet.write(0,21,'Impuesto',format10n)
        sheet.write(0,22,'Monto',format10n)
        sheet.write(0,23,'Total',format10n)
        lines = self.get_invoice_lines(data)
        #print "esto es lines ", lines
        #print "esto es formato ", format10
        ren = 1
        col = 0
        reg= 0
        number=""
        for line in lines:
            sheet.write(ren, col, line['date_invoice'], format10)
            sheet.write(ren, col + 1, line['invoice'], format10)
            if not line['origin']:
                sheet.write(ren, col + 2, "Sin pedido relacionado", format10)
                sheet.write(ren, col + 3, "Sin pedido", format10)
            else:
                pedido = self.env['stock.picking'].search([('name','=',str(line['origin']))])

                if not pedido:
                    pedido=self.env['sale.order'].search([('name','=',str(line['origin']))])
                    sheet.write(ren, col + 2, str(pedido), format10)
                    comercial = pedido.user_id.name
                    sheet.write(ren, col + 3, str(comercial), format10)
                else:
                    pedido = self.env['sale.order'].search([('name','=',str(pedido.origin))])
                    sheet.write(ren, col + 2, str(pedido.name), format10)
                    comercial = pedido.user_id.name
                    sheet.write(ren, col + 3, str(comercial), format10)

            sheet.write(ren, col + 4, line['concept'], format10)
            sheet.write(ren, col + 5, line['client'], format10)
            sheet.write(ren, col + 6, line['coin'], format10cur)
            sheet.write(ren, col + 7, line['tc'], format10cur)
            if(line['state']== 'open'):
                sheet.write(ren, col + 8,'Abierto', format10cur)
            if(line['state']== 'draft'):
                sheet.write(ren, col + 8,'Borrador', format10cur)
            if(line['state']== 'paid'):
                sheet.write(ren, col + 8,'Pagado', format10cur)
            if(line['state']== 'cancel'):
                sheet.write(ren, col + 8,'Cancelado', format10cur)
            sheet.write(ren, col + 9, line['product_code'], format10cur)
            sheet.write(ren, col + 10, line['product'], format10cur)
            sheet.write(ren, col + 11, line['qty'], format10)
            sheet.write(ren, col + 12, line['price'], format10cur)
            sheet.write(ren, col + 13, line['amount'], format10cur)
            sheet.write(ren, col + 14, line['discount'], format10)
            sheet.write(ren, col + 15, line['tax_amount'], format10cur)
            if(number!=line['invoice']):
                sheet.write(ren, col + 16, line['total_amount'], format10cur)
            number=line['invoice']
	    pagos_ = self.get_pagos(line['invoiceid'])
	    pagos_no=1
	    #print pagos_
	    for pagos_l in pagos_:
		#print pagos_l
		if(line['product']==pagos_l['describe']):
	    		sheet.write(ren, col + 17, pagos_l['idpago'], format10cur)
			sheet.write(ren, col + 18, pagos_l['describe'], format10cur)
			sheet.write(ren, col + 19, pagos_l['price'], format10cur)
			sheet.write(ren, col + 20, pagos_l['discount'], format10)
			sheet.write(ren, col + 21, pagos_l['amount'], format10cur)
			sheet.write(ren, col + 22, pagos_l['tax_amount'], format10cur)
			sheet.write(ren, col + 23, line['total_amount']-pagos_l['tax_amount'], format10cur)
			if(pagos_no<len(pagos_)):
				ren = ren + 1
				reg = reg + 1
				pagos_no= pagos_no + 1
	    ren = ren + 1
	    reg = reg + 1



InvoicingReportXls('report.export_lek_invoicinginfo_xls.invoicing_history_report_xls.xlsx', 'account.invoice')
