# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: Jesni Banu(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import datetime
import string
import ast
from odoo.exceptions import UserError
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx

class InvoicingReportXls(ReportXlsx):

    def get_invoice_lines(self, data):
        if data.get('form',False) and data['form'].get('date_start', False) and data['form'].get('date_end', False):
            invoices = []
            lines = []
            invoice_ids = self.env['account.invoice'].search([
                ('date_invoice','>=',data['form'].get('date_start')),
                ('date_invoice','<=',data['form'].get('date_end')),
                ('state','!=','cancel'),
                #('type','=','out_invoice')
                ])
            for invoice_id in invoice_ids:
                invoices.append(invoice_id.id)
            #print "esto es invoice_ids ", invoices
            invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','in',invoices)], order='product_id, invoice_id' )
            #print "esto es invoice_line_ids ", invoice_line_ids
            #invoice_line_ids = invoice_line_ids.search([('product_id', '=', 80)])
            for line_id in invoice_line_ids:
                
                vals = {
                    'invoiceid': line_id.invoice_id.id,
                    'client': line_id.invoice_id.partner_id.name,
                    #'concept': line_id.invoice_id.tax_line_ids.name,
                    'discount': line_id.discount,
                    'coin': line_id.invoice_id.currency_id.name,
                    'tc': 1/line_id.invoice_id.tipocambio,
                    'state': line_id.invoice_id.state,
                    'date_invoice': line_id.invoice_id.date_invoice,
                    'type': line_id.invoice_id.type,
                    'invoice': line_id.invoice_id.number,
                    'qty': line_id.quantity,
                    'product_id':line_id.product_id,
                    'product': line_id.product_id.name,
                    'type_product': line_id.product_id.type,
                    'product_code': line_id.product_id.default_code,
                    'price': (line_id.price_unit),
                    'qty_available_stock': line_id.product_id.qty_available_stock,
                    'amount': (line_id.price_unit) * (line_id.quantity),
                    #'tax_amount': (line_id.invoice_id.amount_tax),
                    'total_amount':line_id.invoice_id.amount_total,
                    'unidad':line_id.uom_id.name,
                    'price_subtotal':line_id.price_subtotal,
                    }
                
                lines.append(vals)

            return lines
    
    def get_pagos(self,invoice):
        pagos_ = []
        id_pago=''
        obj = self.env['account.invoice.line.invoice'].search([('invoice_id', '=', invoice)])
        for pago in obj:
            #print pago
            invoice_line_ids = self.env['account.invoice.line'].search([('invoice_id','=',pago.nc_id.id)])
            #print "esto es invoice_line_ids ", invoice_line_ids
            for line_id in invoice_line_ids:
                if(line_id.invoice_id.state=='open'):
                    id_pago = line_id.invoice_id.number
                    #id_pago = string.replace(id_pago,'account.payment(','')
                    #id_pago = string.replace(id_pago,',)','')
                    vals = {
                        'idpago':id_pago,
                        'comentarios':line_id.invoice_id.comment,
                        'describe':line_id.product_id.name,
                        'discount': line_id.discount,
                        #'amount': line_id.invoice_id.amount_tax,
                        'price': (line_id.price_unit),
                        #'tax_amount': (line_id.price_unit) * (line_id.quantity),
                        }
                    pagos_.append(vals)
        return pagos_

    def generate_xlsx_report(self, workbook, data, lines):
        #print "entra a generate_xls"
        sheet = workbook.add_worksheet('Invoicing Info')
        format1 = workbook.add_format({'font_size': 14, 'bottom': True, 'right': True, 'left': True, 'top': True, 'align': 'vcenter', 'bold': True})
        format11 = workbook.add_format({'font_size': 12, 'align': 'center', 'right': True, 'left': True, 'bottom': True, 'top': True, 'bold': True})
        format10n = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': True})
        format10 = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False})
        format10cur = workbook.add_format({'font_size': 10, 'align': 'center', 'right': True, 'left': True,'bottom': True, 'top': True, 'bold': False,'num_format':'$#,##0.00'})
        format3 = workbook.add_format({'bottom': True, 'top': True, 'font_size': 12})
        font_size_8 = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8})
        red_mark = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 8,
                                        'bg_color': 'red'})
        justify = workbook.add_format({'bottom': True, 'top': True, 'right': True, 'left': True, 'font_size': 12})
        format3.set_align('center')
        font_size_8.set_align('center')
        justify.set_align('justify')
        format1.set_align('center')
        red_mark.set_align('center')
        sheet.write(0,0,'PRODUCTO',format10n)
        sheet.write(0,1,'FECHA',format10n)
        sheet.write(0,2,'CLIENTE/PROVEEDOR',format10n)
        sheet.write(0,3,'FACTURA',format10n)
        sheet.write(0,4,'PRECIO',format10n)
        sheet.write(0,5,'CANTIDAD',format10n)
        sheet.write(0,6,'UNIDAD MEDIDA',format10n)
        sheet.write(0,7,'MONTO',format10n)
        sheet.write(0,8,'SALDO ALMACEN',format10n)

        lines = self.get_invoice_lines(data)
        #print "esto es lines ", lines
        #print "esto es formato ", format10
        ren = 1
        col = 0
        reg= 0
        number=""
        producto = 0
        cantidad = 0
        for line in lines:
            if line['type_product'] != 'product':
               print "Aqui es type product",line['type_product']


            else:
                if line['product_id'] == producto:
                    sheet.write(ren, col, line['product'], format10)
                    sheet.write(ren, col + 1, line['date_invoice'], format10)
                    sheet.write(ren, col + 2, line['client'], format10)
                    sheet.write(ren, col + 3, line['invoice'], format10)
                    sheet.write(ren, col + 4, line['price'], format10cur)
                    sheet.write(ren, col + 5, line['qty'], format10)
                    sheet.write(ren, col + 6, line['unidad'], format10cur)
                    sheet.write(ren, col + 7, line['price_subtotal'], format10cur)
                    if line['type'] == 'in_invoice':
                        sheet.write(ren, col + 8, cantidad , format10)
                        cantidad = cantidad - line['qty']
                    else:
                        sheet.write(ren, col + 8, cantidad, format10)
                        cantidad = cantidad + line['qty']
                else:
                    sheet.write(ren, col, line['product'], format10)
                    sheet.write(ren, col + 1, line['date_invoice'], format10)
                    sheet.write(ren, col + 2, line['client'], format10)
                    sheet.write(ren, col + 3, line['invoice'], format10)
                    sheet.write(ren, col + 4, line['price'], format10cur)
                    sheet.write(ren, col + 5, line['qty'], format10)
                    sheet.write(ren, col + 6, line['unidad'], format10cur)
                    sheet.write(ren, col + 7, line['price_subtotal'], format10cur)
                    sheet.write(ren, col + 8, line['qty_available_stock'], format10)
                    if line['type'] == 'in_invoice':
                        cantidad = line['qty_available_stock'] - line['qty']
                    else:
                        cantidad = line['qty_available_stock'] + line['qty']
                producto = line['product_id']

                ren = ren + 1
                reg = reg + 1



InvoicingReportXls('report.export_lek_invoicinginfo_products.invoicing_history_report_products.xlsx', 'account.invoice')
