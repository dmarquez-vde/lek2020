# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.osv import expression
from odoo.tools.float_utils import float_round
from datetime import datetime
import operator as py_operator

class ProductTemplate(models.Model):
    _inherit = 'product.product'

    qty_available_stock = fields.Float('Quantity Available Stock', compute='_compute_quantities', search='_search_qty_available', digits=dp.get_precision('Product Unit of Measure'),store="True")

    @api.depends('stock_quant_ids', 'stock_move_ids')
    def _compute_quantities(self):
    	print "ENTRA A _COMPUTE_QUANTITIES"
    	res = self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self._context.get('to_date'))
    	for product in self:
    		product.qty_available = res[product.id]['qty_available']
    		product.qty_available_stock = product.qty_available
    		product.incoming_qty = res[product.id]['incoming_qty']
    		product.outgoing_qty = res[product.id]['outgoing_qty']
    		product.virtual_available = res[product.id]['virtual_available']
