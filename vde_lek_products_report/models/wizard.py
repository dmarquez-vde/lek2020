# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<http://www.cybrosys.com>).
#    Author: Jesni Banu(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from odoo import models, fields, api


class InvoicingReport(models.TransientModel):
    _name = "wizard.invoicing.history.products"
    _description = "invoicing History"

    #warehouse = fields.Many2many('stock.warehouse', 'wh_wiz_rel', 'wh', 'wiz', string='Warehouse', required=True)
    #category = fields.Many2many('product.category', 'categ_wiz_rel', 'categ', 'wiz', string='Warehouse')
    date_start = fields.Date(string="Fecha Inicio")
    date_end = fields.Date(string="Fecha Fin")
    currency_id = fields.Many2one('res.currency', string="Moneda", default=3)
    currency_rate = fields.Many2one('res.currency.rate', string="Fecha TC")
    tc = fields.Float(string="Tipo de cambio",digits=(16,4))

    @api.onchange('currency_rate')
    def onchange_currency_rate_id(self, currency_rate):
        #print "esto es currency_rate ", currency_rate
        if currency_rate:
            rate=self.env['res.currency.rate'].browse(currency_rate).rate
            #print "esto es rate ", rate
            #self.tc = 111
            return {'value':{'tc':1/rate}}

    @api.multi
    def export_products(self):
        #print "entra a export_products"
        context = self._context
        datas = {'ids': context.get('active_ids', [])}
        datas['model'] = 'hr.attendance'
        datas['form'] = self.read()[0]
        #print "esto es datas ", datas
        for field in datas['form'].keys():
            #print "esto es field ", field
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]
        if context.get('products_export'):
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'export_lek_invoicinginfo_products.invoicing_history_report_products.xlsx',
                    'datas': datas,
                    'name': 'Reporte (Kardex)'
                    }
